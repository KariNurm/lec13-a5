import express, { Request, Response } from 'express'
import { createProductsTable } from './db'
import productsRouter from './productsRouter'
import { notFound } from './middlewares'

const server = express()
server.use(express.json())

createProductsTable()

server.use('/products', productsRouter)

server.use(notFound)

const PORT = process.env.PORT
server.listen(PORT, () => {
	console.log('Server listening port', PORT)
})

