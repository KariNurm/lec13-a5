import express, {Request, Response}  from 'express'
import dao from './dao'
import { editValidator } from './middlewares'

const productsRouter = express.Router()
//versio
productsRouter.get('/version', async (_req: Request, res: Response ) => {

	res.send('version 1.1')
})
//GET products
productsRouter.get('/', async (_req: Request, res: Response ) => {

	const result = await dao.allProducts()
	const products = result.rows 
	
	res.send(products)
})


//POST a new product
productsRouter.post('/', async (req: Request, res: Response ) => {
	const {name, price} = req.body
	await dao.addProduct(name, price)
	res.status(201).send('New item added!')
})


//GET a single product
productsRouter.get('/:id', async (req: Request, res: Response) => {
	const id = req.params.id
	const result = await dao.singleProduct(id)
	res.send(result.rows)
})


//PUT update a product
productsRouter.put('/:id', editValidator, async (req: Request, res: Response) => {
	const id = req.params.id
	const {name, price} = req.body 
	await dao.editProduct(id, name, price)
	res.send('Product modified!')
})


//DELETE delete a product
productsRouter.delete('/:id', async (req: Request, res: Response) => {
	const id = req.params.id
	await dao.deleteProduct(id)
	res.send('Product deleted!')
})



export default productsRouter