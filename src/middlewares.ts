import { Request, Response, NextFunction } from 'express'

const notFound = (req: Request, res: Response) => {
	res.status(404).send()
}

const editValidator = (req: Request, res: Response, next: NextFunction) => {
	console.log('middleware validator')
	const newData = req.body
	const requiredParams = ['name', 'price']

	for (const param of requiredParams) {
		if (!(param in newData)) {
			return res.status(400).send('invalid request parameters')
		}
	}

	next()
}


export { editValidator, notFound }