import { executeQuery } from './db'

const addProduct = async (name: string, price: number) => {
	const query = 'INSERT INTO products (name, price) VALUES ($1, $2) RETURNING id'
	const params = [name, price]
	const results = await executeQuery(query, params)
	return results
}

const allProducts = async () => {
	const query = 'SELECT * FROM products'
	return await executeQuery(query)
}

const singleProduct = async (id: string) => {
	const query = 'SELECT name, price FROM products WHERE products.id = $1'
	const params = [id]
	return await executeQuery(query, params)
}

const editProduct = async (id: string, name?: string, price?: number) => {
	const query = 'UPDATE products SET name = $2, price = $3 WHERE products.id = $1'
	const params = [id, name, price]
	return await executeQuery(query, params)
}

const deleteProduct = async (id: string) => {
	const query = 'DELETE FROM products WHERE products.id = $1'
	const params = [id]
	return await executeQuery(query, params)
}


export default { allProducts, addProduct, singleProduct, editProduct, deleteProduct }